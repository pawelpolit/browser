package view;

import model.WebsiteParser;
import org.apache.commons.io.FileUtils;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.List;

import static control.Main.previousUrl;
import static control.Main.saveUrl;
import static control.Main.thereIsPreviousUrl;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

/**
 * Created by Pawel Polit
 */

public class WindowView {

    private static final int WINDOW_WIDTH = 1024;
    private static final int WINDOW_HEIGHT = 768;
    private static final int LINK_FIELD_WIDTH = 800;
    private static final int LINK_FIELD_HEIGHT = 40;
    private static final int BACK_BUTTON_WIDTH = 100;
    private static final int LINKS_LIST_WIDTH = 905;
    private static final int LINKS_LIST_HEIGHT = 600;
    private static final int IMAGES_SIZE_HEIGHT = 70;
    private static final int MESSAGE_WIDTH = 400;
    private static final int MESSAGE_HEIGHT = 90;


    private WebsiteParser websiteParser;

    private JTextField linkField;
    private DefaultListModel<String> listModel;
    private JTextField sizeOfImages;
    private JButton backButton;

    public WindowView() {
        websiteParser = new WebsiteParser();

        JFrame window = new JFrame("Browser");
        window.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        window.setDefaultCloseOperation(EXIT_ON_CLOSE);
        window.pack();
        centreWidth(window);
        centreHeight(window);
        window.setResizable(false);
        window.setLayout(new FlowLayout());
        window.getContentPane().setBackground(Color.GRAY);

        window.add(linkPanel());
        window.add(linksList());

        sizeOfImages = new JTextField("Number of images:   Size of images: ");
        sizeOfImages.setBorder(BorderFactory.createEmptyBorder());
        sizeOfImages.setPreferredSize(new Dimension(LINKS_LIST_WIDTH, IMAGES_SIZE_HEIGHT));
        sizeOfImages.setBackground(Color.GRAY);
        sizeOfImages.setFont(new Font(Font.DIALOG, Font.PLAIN, 35));
        sizeOfImages.setEditable(false);
        sizeOfImages.setHorizontalAlignment(JTextField.CENTER);

        window.add(sizeOfImages);

        window.setVisible(true);
    }

    private JPanel linkPanel() {
        JPanel linkPanel = new JPanel();
        linkPanel.setPreferredSize(new Dimension(WINDOW_WIDTH, LINK_FIELD_HEIGHT));
        linkPanel.setBackground(Color.GRAY);
        centreWidth(linkPanel);

        backButton = new JButton();
        backButton.setPreferredSize(new Dimension(BACK_BUTTON_WIDTH, LINK_FIELD_HEIGHT));
        backButton.setFont(new Font(Font.DIALOG, Font.BOLD, 15));
        backButton.setText("BACK");
        backButton.setEnabled(false);
        backButton.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent p_event) {
                try {
                    String previousUrl = previousUrl();
                    linkField.setText(previousUrl);
                    changeUrl(previousUrl);
                    if(!thereIsPreviousUrl()) {
                        ((JButton)p_event.getSource()).setEnabled(false);
                    }
                } catch(Exception p_e) {
                    printMessage(p_e.getMessage());
                }
            }

            @Override
            public void mousePressed(MouseEvent p_event) {
            }

            @Override
            public void mouseReleased(MouseEvent p_event) {
            }

            @Override
            public void mouseEntered(MouseEvent p_event) {
            }

            @Override
            public void mouseExited(MouseEvent p_event) {
            }
        });

        linkField = new JTextField();
        linkField.setPreferredSize(new Dimension(LINK_FIELD_WIDTH, LINK_FIELD_HEIGHT));
        linkField.setFont(new Font(Font.DIALOG, Font.PLAIN, 22));
        linkField.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent p_keyEvent) {
            }

            @Override
            public void keyPressed(KeyEvent p_keyEvent) {
            }

            @Override
            public void keyReleased(KeyEvent p_keyEvent) {

                if(p_keyEvent.getKeyCode() == KeyEvent.VK_ENTER) {
                    try {
                        String newUrl = linkField.getText();
                        changeUrl(newUrl);
                        saveUrl(newUrl);
                        if(thereIsPreviousUrl()) {
                            backButton.setEnabled(true);
                        }

                    } catch(IOException p_e) {
                        printMessage("Connection error");
                    }
                }
            }
        });

        linkPanel.add(backButton);
        linkPanel.add(linkField);
        return linkPanel;
    }

    private JScrollPane linksList() {
        listModel = new DefaultListModel<>();

        JList<String> linksList = new JList<>(listModel);
        linksList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        linksList.setLayoutOrientation(JList.VERTICAL);
        linksList.setFont(new Font(Font.DIALOG, Font.PLAIN, 22));
        linksList.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent p_event) {
                JList links = (JList)p_event.getSource();
                try {
                    int selectedIndex = links.getSelectedIndex();
                    if(selectedIndex >= 0) {
                        String newUrl = listModel.elementAt(selectedIndex);
                        linkField.setText(newUrl);
                        changeUrl(newUrl);
                        saveUrl(newUrl);
                        if(thereIsPreviousUrl()) {
                            backButton.setEnabled(true);
                        }
                    }
                } catch(IOException p_e) {
                    printMessage("Connection error");
                }
            }

            @Override
            public void mousePressed(MouseEvent p_event) {
            }

            @Override
            public void mouseReleased(MouseEvent p_event) {
            }

            @Override
            public void mouseEntered(MouseEvent p_event) {
            }

            @Override
            public void mouseExited(MouseEvent p_event) {
            }
        });

        JScrollPane result = new JScrollPane(linksList);
        result.setPreferredSize(new Dimension(LINKS_LIST_WIDTH, LINKS_LIST_HEIGHT));

        return result;
    }

    private void centreWidth(Component p_component) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int)(dimension.getWidth() - p_component.getWidth()) / 2;
        int y = p_component.getY();
        p_component.setLocation(x, y);
    }

    private void centreHeight(Component p_component) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = p_component.getX();
        int y = (int)(dimension.getHeight() - p_component.getHeight()) / 2;
        p_component.setLocation(x, y);
    }

    private void changeUrl(String p_newUrl) throws IOException {
        websiteParser.changeUrl(p_newUrl);
        setNewLinksList(websiteParser.getLinks());
        List<Long> imagesSizes = websiteParser.getImagesSizes();
        sizeOfImages.setText("Number of images: " +
                                     imagesSizes.size() +
                                     "   Size of images: " +
                                     FileUtils.byteCountToDisplaySize(imagesSizes.stream().mapToLong(Long::longValue).sum()));
    }

    private void setNewLinksList(List<String> p_links) {
        listModel.clear();
        p_links.forEach(listModel::addElement);
    }

    public void printMessage(String p_message) {
        JFrame message = new JFrame("Message");
        message.setUndecorated(true);
        message.getContentPane().setBackground(Color.LIGHT_GRAY);
        message.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        message.setPreferredSize(new Dimension(MESSAGE_WIDTH, MESSAGE_HEIGHT));
        message.setResizable(false);
        message.pack();
        centreWidth(message);
        centreHeight(message);
        message.setAlwaysOnTop(true);
        message.setLayout(new FlowLayout());

        JTextPane messageText = new JTextPane();
        messageText.setPreferredSize(new Dimension(MESSAGE_WIDTH, MESSAGE_HEIGHT / 2));
        messageText.setBackground(Color.LIGHT_GRAY);
        messageText.setText(p_message);
        messageText.setEditable(false);
        messageText.setFont(new Font(Font.DIALOG, Font.PLAIN, 25));

        StyledDocument styledDocument = messageText.getStyledDocument();
        SimpleAttributeSet simpleAttributeSet = new SimpleAttributeSet();
        StyleConstants.setAlignment(simpleAttributeSet, StyleConstants.ALIGN_CENTER);
        styledDocument.setParagraphAttributes(0, styledDocument.getLength(), simpleAttributeSet, false);

        message.add(messageText);

        JButton button = new JButton("OK");
        button.setPreferredSize(new Dimension(MESSAGE_WIDTH / 4, MESSAGE_HEIGHT / 3));
        button.addActionListener(event -> message.dispose());

        message.add(button);
        message.setVisible(true);
    }
}
