package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Pawel Polit
 */

public class HistoryDAO {
    private static final String DRIVER = "org.sqlite.JDBC";
    private static final String DATABASE_PATH = "jdbc:sqlite:src/main/resources/database.sqlite";

    private Connection connection;
    private Statement statement;


    public HistoryDAO() {
        try {
            Class.forName(DRIVER);
        } catch(ClassNotFoundException p_e) {
            throw new RuntimeException("There is no JDBC driver");
        }

        try {
            connection = DriverManager.getConnection(DATABASE_PATH);
            statement = connection.createStatement();
        } catch(SQLException p_e) {
            throw new RuntimeException("Cannot open connection");
        }

        createTableIfDoesNotExist();
    }

    private void createTableIfDoesNotExist() {
        try {
            statement.execute("CREATE TABLE IF NOT EXISTS history (" +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "url VARCHAR(2024)," +
                    "previous INTEGER" +
                    ")");
        } catch(SQLException p_e) {
            throw new RuntimeException("Error during creating table");
        }
    }

    public int getLastAddedUrlId() {
        try {
            ResultSet queryResult = statement.executeQuery("SELECT count(id) FROM history;");
            queryResult.next();
            return queryResult.getInt(1);
        } catch(SQLException p_e) {
            throw new RuntimeException("Error during searching last url id");
        }
    }

    public void insertNewUrl(String p_url, int p_previousUrlId) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO history VALUES (NULL, ?, ?);");
        preparedStatement.setString(1, p_url);
        preparedStatement.setInt(2, p_previousUrlId);
        preparedStatement.execute();
    }

    public int getPreviousToId(int p_id) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT previous FROM history WHERE id = ?;");
            preparedStatement.setInt(1, p_id);
            ResultSet queryResult = preparedStatement.executeQuery();

            if(queryResult.next()) {
                return queryResult.getInt(1);
            } else {
                return 0;
            }
        } catch(SQLException p_e) {
            throw new RuntimeException("Error during reading from history");
        }
    }

    public String getUrlWithId(int p_id) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT url FROM history WHERE id = ?;");
            preparedStatement.setInt(1, p_id);
            ResultSet queryResult = preparedStatement.executeQuery();

            if(queryResult.next()) {
                return queryResult.getString(1);
            } else {
                return null;
            }

        } catch(SQLException p_e) {
            throw new RuntimeException("Error during reading from history");
        }
    }
}
