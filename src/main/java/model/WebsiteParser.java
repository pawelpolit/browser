package model;

import com.google.common.collect.Lists;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

/**
 * Created by Pawel Polit
 */

public class WebsiteParser {

    private URL mainUrl;
    private Element body;

    public void changeUrl(String p_url) throws IOException {
        mainUrl = new URL(p_url);
        URLConnection connection = mainUrl.openConnection();
        InputStream stream = connection.getInputStream();
        Document document = Jsoup.parse(stream, "UTF-8", p_url);
        stream.close();

        body = document.body();
    }

    public List<String> getLinks() {
        return body.getElementsByTag("a").stream()
                .map(a -> a.attr("href"))
                .distinct()
                .filter(url -> !"".equals(url))
                .map(this::fixUrl)
                .collect(Collectors.toList());
    }

    public List<Long> getImagesSizes() {
        CopyOnWriteArrayList<Long> sizesList = Lists.newCopyOnWriteArrayList();
        CopyOnWriteArrayList<Thread> runningThreads = Lists.newCopyOnWriteArrayList();

        List<String> urlsList = body.getElementsByTag("img")
                .stream()
                .map(img -> img.attr("src"))
                .distinct()
                .filter(url -> !"".equals(url))
                .map(this::fixUrl)
                .collect(Collectors.toList());

        urlsList.forEach(url -> new Thread() {
            @Override
            public void run() {
                try {
                    runningThreads.add(this);
                    sizesList.add(new URL(url).openConnection().getContentLengthLong());
                } catch(IOException ignored) {
                } finally {
                    runningThreads.remove(this);
                }
            }
        }.start());

        while(!runningThreads.isEmpty()) {
            try {
                Thread.sleep(1);
            } catch(InterruptedException ignored) {
            }
        }

        return sizesList;
    }

    private String fixUrl(String p_url) {
        if(p_url.startsWith("http")) {
            return p_url;
        } else if(p_url.startsWith("/")) {
            return mainUrl.getProtocol() + "://" + mainUrl.getAuthority() + p_url;
        } else {
            return mainUrl.getProtocol() + "://" + mainUrl.getAuthority() + "/" + p_url;
        }
    }
}
