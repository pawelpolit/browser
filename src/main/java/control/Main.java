package control;

import database.HistoryDAO;
import model.WebsiteParser;
import org.apache.commons.io.FileUtils;
import view.WindowView;

import java.sql.SQLException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * Created by Pawel Polit
 */

public enum Main {
    ;

    private static HistoryDAO historyDAO;
    private static int lastAddedUrlId;
    private static int lastOpenedUrlId;

    public static void main(String[] args) {
        historyDAO = new HistoryDAO();
        lastAddedUrlId = historyDAO.getLastAddedUrlId();
        lastOpenedUrlId = lastAddedUrlId;

        if(args.length == 1 && "console".equals(args[0])) {
            startConsoleApplication();
        } else {
            startWindowApplication();
        }
    }

    private static void startWindowApplication() {
        new WindowView();
    }

    private static void startConsoleApplication() {
        Scanner scanner = new Scanner(System.in);
        String userLink = scanner.nextLine();
        System.out.println();
        WebsiteParser websiteParser = new WebsiteParser();

        while(!"end".equals(userLink)) {
            try {
                boolean back = false;

                if("back".equals(userLink)) {
                    userLink = previousUrl();
                    back = true;
                }

                websiteParser.changeUrl(userLink);
                websiteParser.getLinks().forEach(System.out::println);

                if(!back) {
                    saveUrl(userLink);
                }

                List<Long> imagesSizes = websiteParser.getImagesSizes();

                System.out.println(imagesSizes.size());
                System.out.println(FileUtils.byteCountToDisplaySize(imagesSizes.stream().mapToLong(Long::longValue).sum()));
                System.out.println();
                System.out.println();
            } catch(Exception p_e) {
                System.err.println(p_e.getMessage());
                System.out.println();
            }

            userLink = scanner.nextLine();
            System.out.println();
        }
    }

    public static void saveUrl(String p_url) {
        try {
            historyDAO.insertNewUrl(p_url, lastOpenedUrlId);
        } catch(SQLException p_e) {
            throw new RuntimeException("Error during adding url to database");
        }
        lastOpenedUrlId = ++lastAddedUrlId;
    }

    public static String previousUrl() {
        if(thereIsPreviousUrl()) {
            int previous = historyDAO.getPreviousToId(lastOpenedUrlId);
            String result = historyDAO.getUrlWithId(previous);
            lastOpenedUrlId = previous;
            return result;
        } else {
            throw new NoSuchElementException("There is no previous url");
        }
    }

    public static boolean thereIsPreviousUrl() {
        return lastOpenedUrlId > 1;
    }
}
